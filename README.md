# testdate

[![pipeline status](https://gitlab.com/endran/testdate/badges/master/pipeline.svg)](https://gitlab.com/endran/testdate/commits/master)
[![coverage report](https://gitlab.com/endran/testdate/badges/develop/coverage.svg)](https://gitlab.com/endran/testdate/commits/master)
[![npm version](https://badge.fury.io/js/%40endran%2Ftestdate.svg)](https://badge.fury.io/js/%40endran%2Ftestdate)

Keeping dates in test date fresh and relevant.

## Description

Whenever you have are developing an application that involves dynamic behaviour based on time you run into issues with stale test dates. You need to have dates in your test data, but wever now and then tests may fail due to dates going over some threshold. Those days are gone!<br>
With testdate you can make all your dates in testing relative to now. No limitations of file types, as long as it's plain text.

## Installation

```sh
npm install @endran/testdate --save
```

or

```sh
yarn add @endran/testdate
```

## Usage

Before you seed your database solution with test data run:

```bash
testdate \
    --source myTestData.augmented.csv \
    --destination myTestData.csv \
    --format "YYYY-MM-DD HH:mm:ss"
```

This will scan the `--source` file for keywords, and output fresh data to `--destination`. See https://devhints.io/moment for all `--format` options. Use `--override` an existing destination file, by default it will not override anything.

## Keywords

The `--source` file will be scanned for keywords. You modify dates relative to now, and you can also fix time of day. It's also possible to override formatting for a specific date.

`With fixed time and custom format : __TESTDATE_now-0000-01-00@19:00:00#dddd, MMMM Do YYYY_TESTDATE__...`

Everything between `__TESTDATE_` and `_TESTDATE__` will be considered, folowing these requirements:

-   Starts with either `now+` or `now-`, to indicate to add or substract time relative to now.
-   Required after now, continues with date:
    -   First years, `YYYY` (required).
    -   Then months, `MM`.
    -   Then days, `DD`.
-   Optionally for time, either; `T` for time relative to now; Or `@` for fixed time of day.
    -   Hours, `HH`.
    -   Minutes, `mm`.
    -   Seconds, `ss`.
-   Optionally, to add custom format for a single line, append with `#<yourFormat>`.
-   Each date instance must be on a single line, it may not span 2 lines.

### Example:

Input:

```text
Now                                 : __TESTDATE_now+_TESTDATE__...
Now (LL)                            : __TESTDATE_now+0000#LL_TESTDATE__...
Now in epoch seconds                : __TESTDATE_now+0000@19:00:00#X_TESTDATE__...
Today at 19                         : __TESTDATE_now+0000@19:00:00_TESTDATE__...

Plus 1 month and 5 minutes          : __TESTDATE_now+0000-01-00T00:05:00_TESTDATE__...
Minus 1 month and 5 minutes         : __TESTDATE_now-0000-01-00T00:05:00_TESTDATE__...

+10 Years                           : __TESTDATE_now+0010_TESTDATE__...
With + 5 Months                     : __TESTDATE_now+0010-05_TESTDATE__...
With +10 Days                       : __TESTDATE_now+0010-05-10_TESTDATE__...
With +3 Hours                       : __TESTDATE_now+0010-05-10T03_TESTDATE__...
With +22 Minutes                    : __TESTDATE_now+0010-05-10T03:22_TESTDATE__...
With +16 Seconds                    : __TESTDATE_now+0010-05-10T03:22:16_TESTDATE__...
With Custom Format (LL)             : __TESTDATE_now-0000-01-00T00:05:00#LL_TESTDATE__...
With Fixed Time (19h)               : __TESTDATE_now-0000-01-00@19:00:00_TESTDATE__...
With fixed time and custom format   : __TESTDATE_now-0000-01-00@19:00:00#dddd, MMMM Do YYYY_TESTDATE__...

Multiple per line                   : __TESTDATE_now-0000-01-00@19:00:00#dddd, MMMM Do YYYY_TESTDATE__, and another; __TESTDATE_now+0000_TESTDATE__
```

Output for `testdate --source exampleInput.txt --destination exampleOutput.txt.txt -O -F llll`, ran in CET timezone:

```text
Now                                 : Tue, Dec 18, 2018 9:23 AM...
Now (LL)                            : December 18, 2018...
Now in epoch seconds                : 1545156000...
Today at 19                         : Tue, Dec 18, 2018 7:00 PM...

Plus 1 month and 5 minutes          : Fri, Jan 18, 2019 9:28 AM...
Minus 1 month and 5 minutes         : Sun, Nov 18, 2018 9:18 AM...

+10 Years                           : Mon, Dec 18, 2028 9:23 AM...
With + 5 Months                     : Fri, May 18, 2029 9:23 AM...
With +10 Days                       : Mon, May 28, 2029 9:23 AM...
With +3 Hours                       : Mon, May 28, 2029 12:23 PM...
With +22 Minutes                    : Mon, May 28, 2029 12:45 PM...
With +16 Seconds                    : Mon, May 28, 2029 12:45 PM...
With Custom Format (LL)             : November 18, 2018...
With Fixed Time (19h)               : Sat, Nov 17, 2018 5:00 AM...
With fixed time and custom format   : Saturday, November 17th 2018...

Multiple per line                   : Saturday, November 17th 2018, and another; Tue, Dec 18, 2018 9:35 AM
```

## Help

```sh
Options:
  -V, --version             Output the version number
  -S, --source <path>       Required. Location the source data.
  -D, --destination <path>  Required. Location of output.
  -F, --format <string>     Required. Format of the output.
  -O, --override            Optional. If set will override any existing destination file.
  -Z, --zone                Optional. Set timezone.
  -h, --help                Output usage information

```

See https://momentjs.com/ for more details on formatting and timezones.

## License

Copyright (c) 2020 David Hardy<br>
Copyright (c) 2020 codecentric nl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
