#!/usr/bin/env node

import * as commander from 'commander';
import {Testdate} from './testdate';
import * as fs from 'fs';
import {throws} from 'assert';

const main = async function() {
    const cmd = commander
        .version('0.1.0')
        .option('-S, --source <path>', 'Required. Location the source data.')
        .option('-D, --destination <path>', 'Required. Location of output.')
        .option('-F, --format <string>', 'Required. Format of the output.')
        .option('-O, --override', 'Optional. If set will override any existing destination file')
        .option('-Z, --zone', 'Optional. Set timezone.')
        .parse(process.argv);

    if (cmd.source === undefined) {
        throw Error('--source is required');
    }
    if (cmd.destination === undefined) {
        throw Error('--destination is required');
    }
    if (cmd.format === undefined) {
        throw Error('--format is required');
    }

    if (fs.existsSync(`${process.cwd()}/${cmd.destination}`) && !cmd.override) {
        throw Error(`${cmd.destination} already exsists. Use --override to force override`);
    }

    const success = await new Testdate().start(cmd.source, cmd.destination, cmd.format, cmd.zone);
    console.log(success ? 'Complete successfully' : 'An error occurred');
};

if (require.main === module) {
    main()
        .then(() => {
            console.log(`Finished`);
            process.exit(0);
        })
        .catch(e => {
            console.log(`Finished with error`, e);
            process.exit(e.code || 1);
        });
}
