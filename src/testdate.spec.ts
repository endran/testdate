import {Testdate} from './testdate';
import * as fs from 'fs';
import * as moment from 'moment-timezone';

moment.tz.setDefault('Europe/Amsterdam');

describe('Testdate', () => {
    let testdate: Testdate;

    beforeEach(() => {
        testdate = new Testdate(tz => {
            return moment(1545122120, 'X').tz(tz);
        });
    });

    describe('start', () => {
        it('formats', async () => {
            const output = 'test.txt';

            await testdate.start('exampleInput.txt', output, 'llll', 'CET');

            const expected = fs.readFileSync(`${process.cwd()}/exampleOutput.txt`).toString();
            const actual = fs.readFileSync(`${process.cwd()}/${output}`).toString();
            expect(actual).toEqual(expected);
        });
    });
});
