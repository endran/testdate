import * as fs from 'fs';
import * as readline from 'readline';
import * as moment from 'moment-timezone';

export class Testdate {
    static KEY_START = '__TESTDATE_now';
    static ISO = '2000-01-01T12:00:00';
    static KEY_END = '_TESTDATE__';
    static TARGET_LENGTH = Testdate.KEY_START.length + Testdate.ISO.length + Testdate.KEY_END.length;

    constructor(private now: (tz?: string) => moment.Moment = tz => (tz ? moment() : moment().tz(tz))) {}

    public async start(source: string, destination: string, format: string, tz?: string): Promise<boolean> {
        let handle: {
            resolve?: any;
            reject?: any;
        } = {};

        const res = new Promise((resolve, reject) => {
            handle.resolve = resolve;
            handle.reject = reject;
        });

        const sourceStream = fs.createReadStream(`${process.cwd()}/${source}`);
        const destinationStream = fs.createWriteStream(`${process.cwd()}/${destination}`);

        const rl = readline.createInterface({
            input: sourceStream,
            crlfDelay: Infinity
        });

        rl.on('line', line => {
            destinationStream.write(this.formatLine(line, format, tz) + '\n');
        });

        rl.on('close', () => {
            destinationStream.end(() => {
                handle.resolve(true);
            });
        });

        return res as any;
    }

    private formatLine(line: string, format: string, tz?: string) {
        const index = line.indexOf(Testdate.KEY_START);
        if (index < 0) {
            return line;
        } else {
            const begin = line.substring(0, index);
            const modifier =
                line.substring(index + Testdate.KEY_START.length, index + Testdate.KEY_START.length + 1).indexOf('+') >= 0 ? 1 : -1;
            const content = line.substring(index + Testdate.KEY_START.length + 1, line.indexOf(Testdate.KEY_END));
            const end = line.substring(line.indexOf(Testdate.KEY_END) + Testdate.KEY_END.length);

            const [iso, formatOverride] = content.split('#');
            const [dateTime, fixedTime] = iso.split('@');
            const [date, time] = dateTime.split('T');
            const [years, months, days] = date.split('-');
            const [hours, minutes, seconds] = (time ? time : '').split(':');
            const [fixedHours, fixedMinutes, fixedSeconds] = (fixedTime ? fixedTime : '').split(':');

            const now = this.now(tz);

            if (years) {
                now.set({year: now.year() + modifier * parseInt(years)});
            }
            if (months) {
                now.set({month: now.month() + modifier * parseInt(months)});
            }
            if (days) {
                now.set({date: now.date() + modifier * parseInt(days)});
            }

            if (fixedHours) {
                now.set({hours: modifier * parseInt(fixedHours)});
            } else if (hours) {
                now.set({hours: now.hour() + modifier * parseInt(hours)});
            }
            if (fixedMinutes) {
                now.set({minutes: modifier * parseInt(fixedMinutes)});
            } else if (minutes) {
                now.set({minutes: now.minutes() + modifier * parseInt(minutes)});
            }
            if (fixedSeconds) {
                now.set({seconds: modifier * parseInt(fixedSeconds)});
            } else if (seconds) {
                now.set({seconds: now.seconds() + modifier * parseInt(seconds)});
            }

            let nowMoment = moment(now);
            if (tz) {
                nowMoment = nowMoment.tz(tz);
            }
            const formatted = nowMoment.format(formatOverride ? formatOverride : format);

            return begin + formatted + this.formatLine(end, format, tz);
        }
    }
}
